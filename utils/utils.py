import datetime
import dateutil.parser


def date_converter(obj):
    if isinstance(obj, (datetime.date, datetime.datetime)):
        return obj.isoformat()


def datetime_parser(data):
    for key, value in data.items():
        if isinstance(value, str):
            try:
                date = dateutil.parser.isoparse(value)
                data[key] = date
            except:
                pass
    return data

def pagination(data=None, page_size=None, page=None):
    size = len(data)
    page_size = min(size, page_size)
    offset = page_size*(page-1)
    if offset >= size or page < 1 or page_size < 1:
        return
    first = offset + 1
    if page == 1:
        first = 0
    last = page_size*page + 1
    if last > size:
        last = size
    return data[first:last]
