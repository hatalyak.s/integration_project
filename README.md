### Integration project
Data collection is done with the use of `selenium ChromeDriver`. 
The webdriver fetchs weather history (temperature, dew point,
humidity, wind speed, pressure, precipitations) for specific
dates for capitals of all countries.
`Windows scheduler` was used to run the task daily.
Data source : [wunderground.com](https://www.wunderground.com/). 
### To run scrapper
Set proper settings in `config_example.py` and rename file  to `config.py`.
Run `scrapper/crawl.py`. The data is sent to the `rabbitmq` queue `historical_weather`.
The `db/listener.py` is fetching data from the queue and writing to the `MongoDB`.

Example for 2 capitals:
- logger
```
2020-04-25 17:03:56,765 - INFO - Collecting capitals
2020-04-25 17:04:04,585 - INFO - Collecting city Accra: 1 out of 261
2020-04-25 17:04:24,946 - INFO - Collecting date: 1 out of 2
2020-04-25 17:04:39,556 - INFO - TimeoutException: Failed to find Daily Observations table for https://www.wunderground.com/history/monthly/gh/accra/DGAA
2020-04-25 17:04:39,556 - INFO - Collecting date: 2 out of 2
2020-04-25 17:04:51,244 - INFO - TimeoutException: Failed to find Daily Observations table for https://www.wunderground.com/history/monthly/gh/accra/DGAA
2020-04-25 17:04:51,244 - INFO - Collecting city Adamstown: 2 out of 261
2020-04-25 17:05:08,613 - INFO - Collecting date: 1 out of 2
2020-04-25 17:05:14,401 - INFO - Collecting date: 2 out of 2
```
- data
```
{'city_name': 'Adamstown', 'city_url': 'https://www.wunderground.com/weather/us/md/adamstown', 'monthly_url': 'https://www.wunderground.com/history/monthly/us/md/adamstown/KIAD', 'date': Timestamp('2020-04-20 00:00:00', freq='D'), 'history': {'temperature': {'max': 63.0, 'avg': 54.4, 'min': 48.0}, 'dew_point': {'max': 43.0, 'avg': 40.1, 'min': 37.0}, 'humidity': {'max': 80.0, 'avg': 59.6, 'min': 41.0}, 'wind_speed': {'max': 10.0, 'avg': 5.3, 'min': 0.0}, 'pressure': {'max': 29.4, 'avg': 29.3, 'min': 29.3}, 'precipitations': {'total': 0.0}}}
{'city_name': 'Adamstown', 'city_url': 'https://www.wunderground.com/weather/us/md/adamstown', 'monthly_url': 'https://www.wunderground.com/history/monthly/us/md/adamstown/KIAD', 'date': Timestamp('2020-04-21 00:00:00', freq='D'), 'history': {'temperature': {'max': 66.0, 'avg': 54.2, 'min': 46.0}, 'dew_point': {'max': 45.0, 'avg': 35.3, 'min': 19.0}, 'humidity': {'max': 89.0, 'avg': 53.8, 'min': 23.0}, 'wind_speed': {'max': 31.0, 'avg': 16.3, 'min': 8.0}, 'pressure': {'max': 29.5, 'avg': 29.3, 'min': 29.2}, 'precipitations': {'total': 0.0}}}
```