import React, {Component} from 'react';
import City from './City'

class App extends Component {
  state = {
    page: 1,
    info: []
  }

  fetchData = (pageNum)=>{
    const requestBody = {
      query:
      `query {
            cityByName (name: "Kiev", pageSize: 5, page: ${pageNum}) {
              Id
              date
              cityName
              history{
                temperature{
                  avg
                }
                humidity{
                  avg
                }
                windSpeed{
                  avg
                }
              }
            }
        }`
    };

    return fetch('http://localhost:5000/weathergraph', {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res =>{
      if(res.status!==200 &&res.status!==201) {
        throw new Error('Unable to fetch data');
      }
      return res.json();
    })
    .then(resData => {
      console.log('Info:', resData.data);
      return resData.data
    })
    .catch(err =>{
      console.log(err);
    })
  }

  componentWillMount = ()=>{
    return this.fetchData(1)
    .then(data => {
      return this.setState({
        info: data.cityByName
      })
    })
  }

  nextPage = ()=> {
    return this.fetchData(this.state.page + 1)
    .then(data => {
      return this.setState(prevState=>{
        return{
          info: data.cityByName,
          page: prevState.page + 1
        }})
    })
  }

  previousPage = ()=> {
    return this.fetchData(this.state.page - 1)
    .then(data => {
      return this.setState(prevState=>{
        return{
          info: data.cityByName,
          page: prevState.page - 1
        }})
    })
  }

  render() {
    window.scrollTo(0, 0)
    console.log(this.state);
    return (
      <div className="weather-history">
        {this.state.info.map((day, index) => (
          <City
            cityName={day.cityName}
            history={day.history}
            date={day.date}
            id={day.Id}
            key={day.Id.toString()}
          />
        ))}
        <button className="pagination" onClick={()=>this.nextPage()}>
            <b> NEXT PAGE </b>
        </button>
        <button className="pagination" onClick={()=>this.previousPage()}>
            <b> PREVIOUS PAGE </b>
        </button>
      </div>
    );
  }
}

export default App;
