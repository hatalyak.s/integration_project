import React, { PureComponent } from "react";
import {FaSmile, FaFrown} from 'react-icons/fa'

class City extends PureComponent {
  buttonClicked = (id, like)=>{
    const requestBody = {
      query: `
      query{
        like(observationId: "${id}", like: ${like})
        {
            cityName
        }
       }`
    };
    console.log(requestBody);
    fetch('http://localhost:5000/weathergraph', {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res =>{
      if(res.status!==200 &&res.status!==201) {
        throw new Error('Unable to fetch data');
      }
      return res.json();
    })
    .then(resData => console.log('One record:', resData))
    .catch(err =>{
      console.log(err);
    })
  }


  render() {
    return (
      <div className="city">
        <div className="city-container">
          <h4>
          City: {this.props.cityName}
          </h4>
          <h4>
          Date: {this.props.date}
          </h4>
          <p>
          <b>Average temperature:</b> {this.props.history.temperature.avg}
          </p>
           <p>
          <b>Average humidity:</b> {this.props.history.humidity.avg}
          </p>
           <p>
          <b>Average wind speed:</b> {this.props.history.windSpeed.avg}
          </p>
        </div>
        <div>
          <FaSmile className="smile" size={40} onClick={()=>this.buttonClicked(this.props.id, true)}/>
          <FaFrown className="smile" size={40} onClick={()=>this.buttonClicked(this.props.id, false)}/>
        </div>
      </div>
    );
  }
}
export default City;
