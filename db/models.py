from mongoengine import Document, EmbeddedDocument
from mongoengine.fields import (
    ObjectIdField, DateTimeField, StringField,
    EmbeddedDocumentField, BooleanField, DecimalField)
import config
from mongoengine.fields import *


class Observation(EmbeddedDocument):
    max = FloatField()
    avg = FloatField()
    min = FloatField()


class Precipitations(EmbeddedDocument):
    total = FloatField()


class History(EmbeddedDocument):
    temperature = EmbeddedDocumentField(Observation)
    dew_point = EmbeddedDocumentField(Observation)
    humidity = EmbeddedDocumentField(Observation)
    wind_speed = EmbeddedDocumentField(Observation)
    pressure = EmbeddedDocumentField(Observation)
    precipitations = EmbeddedDocumentField(Precipitations)


class City(Document):
    meta = {'collection': config.COLLECTION}
    _id = ObjectIdField()
    city_name = StringField()
    city_url = StringField()
    monthly_url = StringField()
    date = DateTimeField()
    history = EmbeddedDocumentField(History)
    like = BooleanField()
