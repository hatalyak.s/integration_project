import pika
import json
import config
import logging
from utils import utils
from pymongo import MongoClient

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

logging.basicConfig(level=20, format=config.LOG_FORMAT)

client = MongoClient(config.HOST, config.PORT)
db = client[config.DATABASE]
capitals = db[config.COLLECTION]

channel.queue_declare(queue=config.QUEUE)


def callback(ch, method, properties, body):
    data = json.loads(body, object_hook=utils.datetime_parser)
    logging.info(f"Received data for {data['city_name']}")
    capitals.insert_one(data)


if __name__ == '__main__':
    channel.basic_consume(queue=config.QUEUE,
                          on_message_callback=callback, auto_ack=True)

    logging.info('[*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
