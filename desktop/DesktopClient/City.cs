﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopClient
{
    public class City
    {
        public string Id { get; set; }
        public string CityName { get; set; }
        public DateTime Date { get; set; }
        public History History{ get; set; }
        public bool Like { get; set; }

    }
}
