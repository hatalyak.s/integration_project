﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopClient
{
    public class History
    {
        public Observation Temperature { get; set; }
        public Observation Humidity { get; set; }
        public Observation WindSpeed { get; set; }
    }
}
