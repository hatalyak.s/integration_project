﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQL.Client;
using GraphQL.Common.Request;

namespace DesktopClient
{
    public class ApiConnector
    {
        protected readonly GraphQLClient _client;
        private readonly string _url = "http://localhost:5000/weathergraph";

        public ApiConnector()
        {
            _client = new GraphQLClient(_url);
        }

        public List<City> CityHistory(int page)
        {
            string query = @"
                            query{
                                cityByName (name: ""Kiev"", pageSize: 100, page:1) {
                                Id
                                cityName
                                date
                                  history{
                                    temperature{
                                      avg
                                    }
                                    humidity{
                                      avg
                                    }
                                    windSpeed{
                                      avg
                                    }
                                  }
                                }              
                        }";

            var request = new GraphQLRequest()
            {
                Query = query
            };
            var graphQLResponse = _client.PostAsync(request).Result;
            var weatherHistory = graphQLResponse.GetDataFieldAs<List<City>>("cityByName");
            return weatherHistory;
        }

        public void Mood(string id, bool mood)
        {
            var request = new GraphQLRequest()
            {
                Query = @"
                    query like($observationId:String!, $like: Boolean!){
                            like(observationId: $observationId, like: $like)
                            {
                                cityName
                            }
                    }",
                Variables = new { observationId = id, like = mood }
            };
            var graphQLResponse = _client.PostAsync(request).Result;
        }
    }
}
