﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopClient
{
    public class Observation
    {
        public double Max { get; set; }
        public double Avg { get; set; }
        public double Min { get; set; }
    }
}
