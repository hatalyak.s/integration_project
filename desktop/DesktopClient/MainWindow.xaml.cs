﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DesktopClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ApiConnector _api = new ApiConnector();
        private List<City> history = new List<City>();

        public MainWindow()
        {
            InitializeComponent();
            history = _api.CityHistory(1);
            UpdateListView();
        }

        private void UpdateListView()
        {
            this.historicalDataList.Items.Clear();
            foreach (var day in history)
            {
                Button likeButton = new Button()
                {
                    Name = "like" + day.Id,
                    Width = 50,
                    Height = 50,
                    Content = ":)",
                    Style = FindResource("LikeStyle") as Style,
                    Tag = day.Id,
                    Margin = new Thickness(20, 10, 20, 40)
            };
                Button dislikeButton = new Button()
                {
                    Name = "dislike" + day.Id,
                    Width = 50,
                    Height = 50,
                    Content = ":(",
                    Style = FindResource("LikeStyle") as Style,
                    Tag = day.Id,
                    Margin = new Thickness(20, 10, 20, 40)
            };
                likeButton.Click += new RoutedEventHandler((sender, e) => Mood_Click(sender, e, true));
                dislikeButton.Click += new RoutedEventHandler((sender, e) => Mood_Click(sender, e, false));
                TextBlock text = new TextBlock()
                {
                    FontSize = 20,
                    FontWeight = FontWeights.Bold,
                };
                text.Text = string.Format("City: {0}\n", day.CityName);
                text.Text += string.Format("Date: {0}\n", day.Date.ToString());
                text.Text += string.Format("Average temperature: {0}\n", day.History.Temperature.Avg.ToString());
                text.Text += string.Format("Average humidity: {0}\n", day.History.Humidity.Avg.ToString());
                text.Text += string.Format("Average wind speed: {0}", day.History.WindSpeed.Avg.ToString());

                StackPanel panel = new StackPanel();
                StackPanel buttons = new StackPanel()
                {
                    Orientation = Orientation.Horizontal
                };
                buttons.Children.Add(likeButton);
                buttons.Children.Add(dislikeButton);
                panel.Children.Add(text);
                panel.Children.Add(buttons);
                //this.historicalDataList.Items.Add(text);
                this.historicalDataList.Items.Add(panel);
            }
        }

        private async void Mood_Click(object sender, RoutedEventArgs e, bool like)
        {
            var button = (sender as Button);
            var id = button.Tag.ToString();
            _api.Mood(id, like);
        }
    }
}
