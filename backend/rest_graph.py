from flask import Flask
import graphene
from flask_cors import CORS
from flask_graphql import GraphQLView
from graphene_mongo import MongoengineObjectType
from mongoengine import connect
from utils import utils
from db import models
import config

connect(config.DATABASE, host=config.HOST, port=config.PORT, alias='default')


class City(MongoengineObjectType):
    class Meta:
        model = models.City


class History(MongoengineObjectType):
    class Meta:
        model = models.History


class Observation(MongoengineObjectType):
    class Meta:
        model = models.Observation


class Precipitations(MongoengineObjectType):
    class Meta:
        model = models.Precipitations


class Query(graphene.ObjectType):
    cities = graphene.List(City, page_size=graphene.Int(),
                           page=graphene.Int())
    cityByName = graphene.List(City, name=graphene.String(),
                               page_size=graphene.Int(),
                               page=graphene.Int())
    cityByDate = graphene.List(City, date=graphene.Date(),
                               page_size=graphene.Int(),
                               page=graphene.Int())
    like = graphene.List(City, observation_id=graphene.ID(),
                         like=graphene.Boolean())

    def resolve_cities(self, info, page_size=10, page=1):
        result = list(models.City.objects.all())
        page = utils.pagination(result, page_size=page_size, page=page)
        return page

    def resolve_cityByName(self, info, name, page_size=10, page=1):
        result = list(models.City.objects.filter(city_name=name))
        page = utils.pagination(result, page_size=page_size, page=page)
        return page

    def resolve_cityByDate(self, info, date, page_size=10, page=1):
        result = list(models.City.objects.filter(date=date))
        page = utils.pagination(result, page_size=page_size, page=page)
        return page

    def resolve_like(self, info, observation_id, like):
        models.City.objects(_id=observation_id).update_one(like=like)
        return list(models.City.objects.filter(_id=observation_id))


schema = graphene.Schema(query=Query)

app = Flask(__name__)
CORS(app)

app.add_url_rule(
    '/weathergraph',
    view_func=GraphQLView.as_view('weathergraph', schema=schema, graphiql=True)
)

if __name__ == '__main__':
    app.run(debug=True)
