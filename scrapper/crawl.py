from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException, StaleElementReferenceException
import logging
from datetime import date, timedelta
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import config
import pika
import json
from utils.utils import date_converter
import logging

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
channel.queue_declare(queue=config.QUEUE)

options = webdriver.ChromeOptions()
options.add_argument("headless")
driver = webdriver.Chrome('chromedriver.exe', chrome_options=options)  # chrome_options=options
base_url = 'https://www.wunderground.com/'
capitals_url = 'https://en.wikipedia.org/wiki/List_of_national_capitals'
logging.basicConfig(level=20, format=config.LOG_FORMAT)

MAX_TIMEOUT = 10
TRIPLE_KEYS = ['max', 'avg', 'min']
PRECIP_KEYS = ['total']
IGNORE_EXCEPTIONS = (NoSuchElementException,StaleElementReferenceException,)
MAX_RETRY = 2


def get_capitals():
    """
    Collect capital names from wikipedia page
    Returns:
        capitals(list of str)
    """
    driver.get(f'{capitals_url}')
    logging.info(f'Sending request: {capitals_url}')
    capitals = [element.get_attribute('innerText') for element
                in driver.find_elements_by_xpath("//table[contains(@class, 'wikitable')]//td[1]//a")]
    return capitals


def get_city_url(city_name, retries=0):
    """
    Paste city name into search input and navigate to the city page
    Args:
        city_name(str)
    Returns:
        city_url(str)
    """
    driver.get(f'{base_url}')
    search = driver.find_element_by_id('wuSearch')
    search.clear()
    search.send_keys(city_name)
    try:
        clicker = WebDriverWait(driver, MAX_TIMEOUT, ignored_exceptions=IGNORE_EXCEPTIONS).until(
            EC.presence_of_element_located((By.XPATH, "//li[contains(@class,'result-type-city')]/following::a")))
        clicker.click()
        url_change = WebDriverWait(driver, MAX_TIMEOUT).until(EC.url_changes(base_url))
        if url_change:
            city_url = driver.current_url
            return city_url
        else:
            raise TimeoutException
    except StaleElementReferenceException:
        if retries < MAX_RETRY:
            logging.info(f'Retrying to get city url for {city_name}')
            get_city_url(city_name, retries+1)
        else:
            logging.error(f'StaleElementReferenceException: element is not attached to the page document {city_name}')
    except TimeoutException:
        logging.error(f'TimeoutException: Failed to navigate to the url of {city_name}')


def get_monthly_url(city_url, retries=0):
    """
    Navigate to the monthly historic data and return the url
    Args:
        city_url(str)
    Returns:
        monthly_url(str)
    """
    driver.get(f'{city_url}')
    logging.info(f'Sending request: {city_url}')
    try:
        history_link_element = WebDriverWait(driver, MAX_TIMEOUT).until(
            EC.presence_of_element_located((By.XPATH, "//li[@class='ng-star-inserted']//a[contains(., 'History')]")))
        history_link_element.click()
        monthly_link_element = WebDriverWait(driver, MAX_TIMEOUT).until(
            EC.presence_of_element_located((By.XPATH, "//a[contains(., 'Monthly')]")))
        monthly_url = monthly_link_element.get_attribute('href')
        return monthly_url
    except StaleElementReferenceException:
        if retries < MAX_RETRY:
            logging.info(f'Retrying to get monthly url for {city_url}')
            get_city_url(city_url, retries+1)
        else:
            logging.error(f'StaleElementReferenceException: element is not attached to the page document {city_url}')
    except TimeoutException:
        logging.error(f'TimeoutException: unable to get monthly url for {city_url}')


def table_parser(table, row, column, keys):
    """
    Parse table's row inside specific column
    """
    values = table.find_element_by_xpath(f"//td[{column}]//tr[{row}]")
    values = [float(val) for val in values.get_attribute('textContent').split()]
    dictionary = dict(zip(keys, values))
    return dictionary


def get_historic_data(monthly_url, date):
    """
    Fetch weather history for specific date from Daily Observations table within monthly history page
    Args:
       monthly_url(str)
       date(date)
    Returns:
       daily_info(dict)
    """
    driver.get(f'{monthly_url}/date/{date.year}-{date.month}')
    logging.info(f'Sending request: {monthly_url}/date/{date.year}-{date.month}')
    try:
        daily_table = WebDriverWait(driver, MAX_TIMEOUT).until(
            EC.presence_of_element_located((By.XPATH, "//table[contains(@class, 'days')]/tbody/tr")))
    except TimeoutException:
        logging.info(f'TimeoutException: Failed to find Daily Observations table for {monthly_url}')
        return
    row = date.day + 1
    daily_info = dict()
    keys = ['temperature', 'dew_point', 'humidity', 'wind_speed', 'pressure', 'precipitations']
    for index, key in enumerate(keys):
        if key == 'precipitations':
            daily_info[key] = table_parser(daily_table, row, index + 2, PRECIP_KEYS)
        else:
            daily_info[key] = table_parser(daily_table, row, index + 2, TRIPLE_KEYS)
    return daily_info


def collect_data_between_dates(start_date='2020-04-20', end_date='2020-04-21'):
    import pandas as pd
    dates = pd.date_range(start_date, end_date, freq='D')
    return run(dates)


def run(dates):
    """
    Fetch weather history (temperature, dew_point, humidity, wind_speed, pressure, precipitations)
    for specific dates for capitals of all countries
    """
    logging.info('Collecting capitals')
    capitals = get_capitals()
    capitals_count = len(capitals)
    dates_count = len(dates)
    for city_index, city_name in enumerate(capitals):
        logging.info(f'Collecting city {city_name}: {city_index + 1} out of {capitals_count}')
        document = dict()
        document['city_name'] = city_name
        city_url = get_city_url(city_name)
        if not city_url:
            continue
        document['city_url'] = city_url
        monthly_url = get_monthly_url(city_url)
        if not monthly_url:
            continue
        document['monthly_url'] = monthly_url
        for date_index, date in enumerate(dates):
            logging.info(f'Collecting date: {date_index + 1} out of {dates_count}')
            doc = document.copy()
            doc['date'] = date
            historic_data = get_historic_data(monthly_url, date)
            if not historic_data:
                continue
            doc['history'] = historic_data
            # Send collected data to the queue
            channel.basic_publish(exchange='', routing_key=config.QUEUE, body=json.dumps(doc, default=date_converter))
            logging.info(f'Sent data for {city_name} to the queue')


if __name__ == '__main__':
    today = date.today() - timedelta(days=1)  # get yesterday history
    # collect_data_between_dates()
    run([today])
    driver.close()
