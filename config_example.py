QUEUE = 'weather'
LOG_FORMAT = '%(asctime)s - %(levelname)s - %(message)s'
HOST = 'localhost'
PORT = 27017
DATABASE = 'historical_weather'
COLLECTION = 'capitals'
